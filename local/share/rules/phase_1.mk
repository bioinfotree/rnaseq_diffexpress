# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

# transcript fasta file
REFERENCE ?=

# file containing 'gene(tab)transcript' identifiers per line
GENE2TRANS_MAPPING ?=

# bowtie|bowtie2|(path to bam file) alignment method.  (note: RSEM requires bowtie)
# (if you already have a bam file, you can use it here instead of rerunning bowtie)
ALN_METHOD ?= bowtie

# RSEM|eXpress abundance estimation method
EST_METHOD ?= RSEM

# must be initialize as empty
SAMPLES ?=

logs:
	mkdir -p $@

# reference transcriptome
transcript.reference.fasta.gz:
	ln -sf $(REFERENCE) $@


ifdef GENE2TRANS_MAPPING
transcript.reference.fasta.gene_trans_map:
	ln -sf $(GENE2TRANS_MAPPING) $@
endif

# this function install all the links at once
%.fastq.gz:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(foreach KEY,$(call keys,$(SAMPLE)), \
			$(shell ln -sf $(call get,$(SAMPLE),$(KEY)) $(SAMPLE).reads.$(KEY).fastq.gz) \
		) \
	)

# for unzip file contents
%.fastq: %.fastq.gz
	if [ -s $< ]; then \   * if use single reads, right file is needed by %.isoforms.results.ok target. Here create a fake %.reads.RIGHT.fastq by touching it *
		zcat <$< >$@; \
	else \
		touch $@; \
	fi

%.fasta: %.fasta.gz
	if [ -s $< ]; then \
		zcat <$< >$@; \
	else \
		touch $@; \
	fi


# only prepare the reference for the given abundance estimation method and aligner
define PREPARE_REFERENCE
	ADD_OPTS=""; \
	if [ "$(ALN_METHOD)" == "bowtie" ] && [ "$(ALN_METHOD_ADD_OPTS)" != "" ] && [ "$(EST_METHOD)" == "RSEM" ]; then \
	\
		ADD_OPTS="$$ADD_OPTS --bowtie_RSEM \"$(ALN_METHOD_ADD_OPTS)\""; \
	\	
	elif [ "$(ALN_METHOD)" == "bowtie" ] && [ "$(ALN_METHOD_ADD_OPTS)" != "" ] && [ "$(EST_METHOD)" == "eXpress" ]; then \
	\
		ADD_OPTS="$$ADD_OPTS --bowtie_eXpress \"$(ALN_METHOD_ADD_OPTS)\""; \
	\	
	elif [ "$(ALN_METHOD)" == "bowtie2" ] && [ "$(ALN_METHOD_ADD_OPTS)" != "" ] && [ "$(EST_METHOD)" == "RSEM" ]; then \
	\
		ADD_OPTS="$$ADD_OPTS --bowtie2_RSEM \"$(ALN_METHOD_ADD_OPTS)\""; \
	\	
	elif [ "$(ALN_METHOD)" == "bowtie2" ] && [ "$(ALN_METHOD_ADD_OPTS)" != "" ] && [ "$(EST_METHOD)" == "eXpress" ]; then \
	\
		ADD_OPTS="$$ADD_OPTS --bowtie2_eXpress \"$(ALN_METHOD_ADD_OPTS)\""; \
	fi; \
	$(call module_loader); \
	align_and_estimate_abundance.pl \
	--seqType fq \
	--transcripts $1 \
	--est_method $(EST_METHOD) \
	--aln_method $(ALN_METHOD) \
	--prep_reference \
	$$ADD_OPTS
endef



# align_and_estimate_abundance.pl --transcripts transcript.reference.fasta --est_method RSEM --aln_method bowtie  --bowtie_RSEM "--all --best --strata -m 300 --chunkmbs 512" --prep_reference
# align and estimate abundance for each sample
define ALIGN_AND_ESTIMATE_ABUNDANCE
	ADD_OPTS=""; \
	if [ "$(EST_METHOD)" == "RSEM" ] && [ "$(EST_METHOD_ADD_OPTS)" != "" ]; then \
	\
		ADD_OPTS="$$ADD_OPTS --rsem_add_opts \"$(EST_METHOD_ADD_OPTS)\""; \
	\
	elif [ "$(EST_METHOD)" == "eXpress" ] && [ "$(EST_METHOD_ADD_OPTS)" != "" ]; then \
	\
	\	ADD_OPTS="$$ADD_OPTS --eXpress_add_opts \"$(EST_METHOD_ADD_OPTS)\""; \
	fi; \
	if [ -z "$(SINGLE_END)" ]; then \
		ADD_OPTS="$$ADD_OPTS --left $2 --right $3"; \   * use paired-ends if SINGLE_END is not defined *
	else \
		ADD_OPTS="$$ADD_OPTS --single $2"; \   * use single-end if SINGLE_END is defined as True *
	fi; \
	echo $$ADD_OPTS; \
	$(call module_loader); \
	align_and_estimate_abundance.pl \
	--transcripts $1 \
	--seqType fq \
	--est_method $(EST_METHOD) \
	--aln_method $(ALN_METHOD) \
	--thread_count $$THREADNUM \
	--output_dir $4 \
	--output_prefix $(call first,$(call split,.,$5)) \   * get the prefix *
	$$ADD_OPTS
endef



# test if GENE2TRANS_MAPPING exists, then use it
ifdef GENE2TRANS_MAPPING

transcript.reference.fasta.$(ALN_METHOD).ok: logs transcript.reference.fasta transcript.reference.fasta.gene_trans_map
	$(call PREPARE_REFERENCE,$^2) \
	--gene_trans_map $^3 2>&1 \
	| tee $</align_and_estimate_abundance.pl.$@.log

%.isoforms.results.ok: transcript.reference.fasta.$(ALN_METHOD).ok transcript.reference.fasta %.reads.LEFT.fastq %.reads.RIGHT.fastq transcript.reference.fasta.gene_trans_map logs
	!threads
	$(call ALIGN_AND_ESTIMATE_ABUNDANCE,$^2,$^3,$^4,$*,$@) \
	--gene_trans_map $^5 2>&1 \
	| tee $^6/align_and_estimate_abundance.pl.$@.log

else

# otherwise use --trinity_mode to automatically generate the gene_trans_map and use it
transcript.reference.fasta.$(ALN_METHOD).ok: logs transcript.reference.fasta
	$(call PREPARE_REFERENCE,$^2) \
	--trinity_mode 2>&1 \
	| tee $</align_and_estimate_abundance.pl.$@.log

%.isoforms.results.ok: transcript.reference.fasta.$(ALN_METHOD).ok transcript.reference.fasta %.reads.LEFT.fastq %.reads.RIGHT.fastq logs
	!threads
	$(call ALIGN_AND_ESTIMATE_ABUNDANCE,$^2,$^3,$^4,$*,$@) \
	--trinity_mode 2>&1 \
	| tee $^5/align_and_estimate_abundance.pl.$@.log
endif



# for testing purpose
.PHONY: test
test:
	@echo $(call get,PIPPO,KEY1)
	@echo $(call get,PIPPO,KEY2)
	@echo $(call keys,PIPPO)
	@echo $(call defined,PIPPO,KEY1)
	@echo $(call defined,PIPPO,KEY3)

.PHONY: test2
test2:
	ADD_OPTS=""; \
	if [ "$(ALN_METHOD)" == "bowtie" ] && [ "$(ALN_METHOD_ADD_OPTS)" != "" ] && [ "$(EST_METHOD)" == "RSEM" ]; then \
	\
		ADD_OPTS="$$ADD_OPTS --bowtie_RSEM \"$(ALN_METHOD_ADD_OPTS)\""; \
	\
	elif [ "$(ALN_METHOD)" == "bowtie" ] && [ "$(ALN_METHOD_ADD_OPTS)" != "" ] && [ "$(EST_METHOD)" == "eXpress" ]; then \
	\
		ADD_OPTS="$$ADD_OPTS --bowtie_eXpress \"$(ALN_METHOD_ADD_OPTS)\""; \
	\
	elif [ "$(ALN_METHOD)" == "bowtie2" ] && [ "$(ALN_METHOD_ADD_OPTS)" != "" ] && [ "$(EST_METHOD)" == "RSEM" ]; then \
	\
		ADD_OPTS="$$ADD_OPTS --bowtie2_RSEM \"$(ALN_METHOD_ADD_OPTS)\""; \
	\
	elif [ "$(ALN_METHOD)" == "bowtie2" ] && [ "$(ALN_METHOD_ADD_OPTS)" != "" ] && [ "$(EST_METHOD)" == "eXpress" ]; then \
	\
		ADD_OPTS="$$ADD_OPTS --bowtie2_eXpress \"$(ALN_METHOD_ADD_OPTS)\""; \
	fi; \
	align_and_estimate_abundance.pl \
	--transcripts $< \
	--est_method $(EST_METHOD) \
	--aln_method $(ALN_METHOD) \
	--prep_reference \
	$$ADD_OPTS



ALL += $(addsuffix .isoforms.results.ok,$(SAMPLES)) \
	logs


INTERMEDIATE += 


CLEAN += $(wildcard transcript.reference.*) \
	 $(SAMPLES)